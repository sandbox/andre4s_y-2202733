Description
-----------
This module adds a proxy configuration file page to your Drupal site.
Proxy configuration file can be a Proxy Auto-Config (proxy.pac) file or a Web 
Proxy Autodiscovery Protocol (wpad.dat) file or both. These files will have 
correct configurable mimetype application/x-ns-proxy-autoconfig or
application/x-javascript-config. Configuration file will available at: 
$base_url/proxy.pac or $base_url/wpad.dat or both.

Rules when create configuration file:
1. Comment line will not be included in the output.
2. Must contain function FindProxyForURL(url, host).
3. Must return a value of type String.
4. Use spaces instead of tabs.

More information related proxy automatic configuration files:
1. pac: http://en.wikipedia.org/wiki/Proxy_auto-config
2. wpad: http://en.wikipedia.org/wiki/Web_Proxy_Autodiscovery_Protocol

Requirements
------------
Drupal 7.x

Installation
------------
1. Copy the entire proxy_auto directory the Drupal sites/all/modules directory.
2. Login as an administrator. Enable the module in the "Administration" -> 
"Modules"
3. Edit the proxy configuration file under "Administration" -> "Configuration" 
-> "System" -> "Proxy Auto Settings"
