<?php
/**
 * @file
 * Provide form setting, validate, and submit function for proxy_auto module.
 */

/**
 * Function proxy_auto_settings().
 *
 * Provide form to modify proxy configuration file. Admin only.
 *
 * @return array
 *   System Setting Form.
 */
function proxy_auto_settings() {
  // Define help for configuration type.
  $description = t('Default path for Proxy Auto Config is <strong>proxy.pac</strong>');
  $description .= '<br />';
  $description .= t('Default path for Web Proxy Autodiscovery Protocol is <strong>wpad.dat</strong>.');
  // PAC or WPAD?
  $form['proxy_auto_type'] = array(
    '#type' => 'select',
    '#title' => t('Proxy Auto Type'),
    '#required' => TRUE,
    '#multiple' => FALSE,
    '#options' => array(
      'proxy.pac' => t('Proxy Auto Config'),
      'wpad.dat' => t('Web Proxy Autodiscovery Protocol'),
      'both' => t('Both'),
    ),
    '#default_value' => variable_get('proxy_auto_type', 'both'),
    '#description' => $description,
  );
  // Define help for configuration content.
  $description = t('Rules:');
  $description .= '<ul>';
  $description .= '<li>' . t('Comment line will not be included in the output.') . '</li>';
  $description .= '<li>' . t('Must contain function FindProxyForURL(url, host).') . '</li>';
  $description .= '<li>' . t('Must return a value of type String.') . '</li>';
  $description .= '<li>' . t('Use spaces instead of tabs.') . '</li>';
  $description .= '</ul>';
  // Get proxy configuration content.
  $proxy_auto_content = variable_get('proxy_auto_content', '');
  // Get how many lines inside current content.
  $proxy_auto_content_rows = preg_match_all('#\n#', $proxy_auto_content, $matches);
  // Proxy configuration content.
  $form['proxy_auto_content'] = array(
    '#title' => t('Proxy Auto Content'),
    '#type' => 'textarea',
    '#required' => TRUE,
    '#default_value' => $proxy_auto_content,
    '#rows' => ((int) $proxy_auto_content_rows >= 5) ? ++$proxy_auto_content_rows : 5,
    '#description' => $description,
  );
  // Proxy configuration mimetype.
  $form['proxy_auto_mimetype'] = array(
    '#type' => 'select',
    '#title' => t('Proxy Auto Mimetype'),
    '#required' => TRUE,
    '#multiple' => FALSE,
    '#options' => array(
      'application/x-ns-proxy-autoconfig' => t('application/x-ns-proxy-autoconfig'),
      'application/x-javascript-config' => t('application/x-javascript-config'),
    ),
    '#default_value' => variable_get('proxy_auto_mimetype', 'application/x-ns-proxy-autoconfig'),
    '#description' => t('It would be reasonable to assume that <strong>application/x-ns-proxy-autoconfig</strong> will be supported in more clients than <strong>application/x-javascript-config</strong> as it was defined in the original Netscape specification, the latter type coming into use more recently.'),
  );
  // Return system setting form: auto set variables.
  return system_settings_form($form);
}

/**
 * Function proxy_auto_settings_validate().
 *
 * Check for proxy configuration file content:
 * - Check for function FindProxyForURL.
 * - Check for at least one return value.
 *
 * @param array $form
 *   Form array.
 * @param array $form_state
 *   Form values array.
 */
function proxy_auto_settings_validate(array $form, array &$form_state) {
  // Checking content: check function FindProxyForURL.
  if (strpos($form_state['values']['proxy_auto_content'], 'FindProxyForURL') === FALSE) {
    form_set_error('proxy_auto_content', t('Proxy configuration content must use function FindProxyForURL.'));
  }
  // Checking content: must at least has one return.
  if (strpos($form_state['values']['proxy_auto_content'], 'return') === FALSE) {
    form_set_error('proxy_auto_content', t('Proxy configuration content must have at least one return value of type String.'));
  }
}

/**
 * Function proxy_auto_settings_submit().
 *
 * Modification for proxy configuration file content:
 * - Remove unnecessary carriage return char (\r).
 * - Remove unnecessary tab char (\t).
 * - Remove unnecessary multiple vertical spaces.
 *
 * @param array $form
 *   Form array.
 * @param array $form_state
 *   Form values array.
 */
function proxy_auto_settings_submit(array $form, array &$form_state) {
  // Remove unnecessary vertical spacing: \n.
  $form_state['values']['proxy_auto_content'] = preg_replace('#\r#', '', $form_state['values']['proxy_auto_content']);
  // Remove tab, change it with 4 spaces.
  $form_state['values']['proxy_auto_content'] = preg_replace('#\t#', '    ', $form_state['values']['proxy_auto_content']);
  // Remove double vertical space.
  $form_state['values']['proxy_auto_content'] = preg_replace('#\v+#', "\n", $form_state['values']['proxy_auto_content']);
}
