<?php
/**
 * @file
 * Provide callback for print proxy content as defined at hook menu.
 */

/**
 * Function proxy_auto_default().
 *
 * Provide content for proxy configuration file with correct mimetype.
 */
function proxy_auto_default() {
  // Get proxy auto type.
  $proxy_auto_type = variable_get('proxy_auto_type', 'both');
  // Get proxy_auto_content.
  $proxy_auto_content = variable_get('proxy_auto_content', '');
  // Check whether pac or wpad.
  // Check whether configuration content is not empty.
  if (($proxy_auto_type != 'both' && $proxy_auto_type != request_path())
    || empty($proxy_auto_content)
  ) {
    return drupal_not_found();
  }
  // Enable cache on this page.
  drupal_page_is_cacheable(TRUE);
  // Erase and turn off output buffering mechanism.
  ob_end_clean();
  // Set header with selected mimetype.
  header('Content-Type: ' . variable_get('proxy_auto_mimetype', 'application/x-ns-proxy-autoconfig'));
  // Remove comment line.
  $proxy_auto_content = preg_replace('#\s*\/\/[^\n]*#', '', $proxy_auto_content);
  // Print proxy auto content.
  echo $proxy_auto_content;
  // Exiting in proper way.
  drupal_exit();
}
